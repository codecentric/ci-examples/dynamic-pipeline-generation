#!/bin/bash

DYN_CHILD_PIPELINE_FILE="dynamic-child.gitlab-ci.yml"

# HEADER
cat > ${DYN_CHILD_PIPELINE_FILE} <<EOF
stages:
  - build
EOF

# DYNAMIC JOBS
for i in $(seq 1 10);
do
    cat >> ${DYN_CHILD_PIPELINE_FILE} <<EOF
    
"job $i":
  stage: build
  script:
    - echo "this is job $1"
EOF
done