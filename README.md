# Dynamic Generation Of Child Pipelines

This project demonstrates how one can create pipeline definitions at runtime and execute them.

The script `generate-child-pipeline.sh` creates a file `dynamic-child.gitlab-ci.yml` that gets exported as an artifact in the first pipeline step.

The second pipeline step uses this artifact to trigger a child pipeline defined by the artifact.